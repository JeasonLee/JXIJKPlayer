Pod::Spec.new do |s|

    s.name         = "JXIJKPlayer"
    s.version      = "0.1.0"
    s.summary      = "IJKPlayer SSL Framework."
    s.homepage     = "https://gitlab.com/JeasonLee/JXIJKPlayer"
    s.license      = { :type => "GNU Lesser General Public License v2.1", :text => <<-LICENSE
		   GNU LESSER GENERAL PUBLIC LICENSE
		   Version 2.1, February 1999
		   https://gitlab.com/JeasonLee/JXIJKPlayer/blob/master/LICENSE
                 LICENSE
               }
    s.author       = { "Jeason" => "jeason.l@qq.com" }
    s.platform     = :ios
    s.source       = { :git => "https://JeasonLee@gitlab.com/JeasonLee/JXIJKPlayer.git", :tag => "#{s.version}" }
  
    s.source_files = "JXIJKPlayer/*"
  
    s.ios.vendored_frameworks = 'JXIJKPlayer/*.framework'
    s.ios.vendored_library = "JXIJKPlayer/*.a"
    s.frameworks  = "AudioToolbox", "AVFoundation", "CoreGraphics", "CoreMedia", "CoreVideo", "MobileCoreServices", "OpenGLES", "QuartzCore", "VideoToolbox", "Foundation", "UIKit", "MediaPlayer"
    s.libraries   = "bz2", "z", "stdc++"
  
    s.requires_arc = true
    s.platform = :ios, "8.0"
  
  end
  